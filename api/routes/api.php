<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\PlaceController;
use App\Http\Controllers\ReportController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
// Rutas para el ciudades //
Route::get('city', [PlaceController::class, 'index']);
Route::get('city/{id}', [PlaceController::class, 'show']);
Route::post('city', [PlaceController::class, 'store']);
Route::put('city/{id}', [PlaceController::class, 'update']);
Route::delete('city/{id}', [PlaceController::class, 'destroy']);
// Rutas para el ciudades //

// Rutas para el cliente //
Route::get('customer', [CustomerController::class, 'index']);
Route::get('customer/{id}', [CustomerController::class, 'show']);
Route::post('customer', [CustomerController::class, 'store']);
Route::put('customer/{id}', [CustomerController::class, 'update']);
Route::delete('customer/{id}', [CustomerController::class, 'destroy']);
// Rutas para el cliente //


// Rutas para el reporte //

Route::get('report/{id}', [ReportController::class, 'download']);

// Rutas para el reporte //