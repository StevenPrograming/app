<?php

namespace App\Http\Controllers;

use App\Http\Requests\CustomerCreate;
use App\Http\Requests\CustomerUpdate;
use App\Models\Customer;
use Illuminate\Http\Request;



class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      return Customer::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = new Customer();
      $dates = $request->only($data->fillable);
      Customer::create($dates);
      return response()->json([
        'res' => true,
        'title' => 'Registro creado con exito'
      ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return Customer::where('id', $id)->with('city')->get();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerUpdate $request, $id)
    {
      $data = new Customer();
      $edit = Customer::where('id', $id)->update($request->only($data->fillable));
      return response()->json([
        'res' => true,
        'title' => 'Registro modificado con exito'
      ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      return Customer::destroy($id);
    }
}
