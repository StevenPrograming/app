<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';
    protected $fillable = [
        'name', 'fullname', 'dni', 'fn', 'direction', 'status', 'email', 'city'
    ];
    protected $hidden = [
        'created_at', 'updated_at'
    ];
    function city() {
        return $this->hasOne(Place::class, 'id', 'city');
    }
}
