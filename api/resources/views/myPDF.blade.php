<!DOCTYPE html>

<html>

<head>

    <title>Reporte</title>

</head>
<body>

  <h1>{{ $title }}</h1>

  <p>{{ $date }}</p> 

    <div class="row">
         <table style="width: 100%;border: 1px solid #000;">
             <tr>
                 <th>Nombre</th>
                 <th>Apellidos</th>
                 <th>DNI</th>
                 <th>Estatus</th>
             </tr>
             @foreach ($dates as $info)
             <tr>
                 <td style="width: 25%; text-align: left; vertical-align: top; padding: 0.3em;caption-side: bottom;background: #eee;">{{ $info->name }}</td>
                 <td style="width: 25%; text-align: left; vertical-align: top; padding: 0.3em;caption-side: bottom;background: #eee;">{{ $info->fullname }}</td>
                 <td style="width: 25%; text-align: left; vertical-align: top; padding: 0.3em;caption-side: bottom;background: #eee;">{{ $info->dni }}</td>
                 <td style="width: 25%; text-align: left; vertical-align: top; padding: 0.3em;caption-side: bottom;background: #eee;">{{ $info->status ? 'Activo' : 'Inactivo' }}</td>
             </tr>
             @endforeach
         </table>
     </div>

</body>

</html>