<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            [
                'name' => 'Bairon Steven',
                'fullname' => 'Gonzalez Garcia',
                'dni' => '26532622',
                'fn' => '1998-09-25',
                'direction' => 'CAlle 4, c/c Prueba de GeorApp',
                'status' => false,
                'email' => 'bgonzales.wx@gmail.com'
            ]
        ]);
    }
}
