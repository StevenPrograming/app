Herramientas a utilizar para la ejecucion del app de prueba para la vacante de Georp
Vuejs, Composer Laravel, MySqldb

Una vez clonado el repositorio se debe de ejecutar:

Para el front (carpeta client)
npm install
(Instaladas las dependecias se ejecuta el comando del levantamiento del servicio)
npm run serve

Para el Back (carpeta api)
composer install
** Chequea y configura tu archivo .env
php artisan key:generate
php artisan migrate
(Instaladas las dependecias se ejecuta el comando del levantamiento del servicio)
php artisan serve

