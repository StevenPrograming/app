/*
* Archivo con la configuración general de axios
*/
import Vue from 'vue'
import axios from 'axios'
import env from '../env'

const instancia = axios.create({
  baseURL: env.apiUrl// url base cargada de archivo env.js
})

export default async (/* { store } */) => {
  // Vue.prototype.$axios = axios
  Vue.prototype.$api = instancia

  instancia.interceptors.response.use(function (response) {
    console.log('axiosResponse', response)
    // Todo bien con la respuesta
    if (response.config.method === 'post') {
      if (response.status === 204) {
        if (response.data.token === undefined) { // Si no es login
          /* this.$vs.notification({
            color: 'success',
            position: 'top-right',
            title: 'Registro guardado con éxito'
          }) */
        } else { // Es Login
          /* let sessionInfo = {
            token: response.data.token
          } */
          localStorage.setItem('sessionInfo', JSON.stringify(response.data))
        }
      }
      if (response.status === 200) {
        if (response.data.token === undefined) { // Si no es login
          /* this.$vs.notification({
            color: 'success',
            position: 'top-right',
            title: 'Registro modificado con éxito'
          }) */
        } else { // Es Login
          /* let sessionInfo = {
            token: response.data.token
          } */
          localStorage.setItem('sessionInfo', JSON.stringify(response.data))
        }
      }
      if (response.status === 501) {
        if (response.data.token === undefined) { // Si no es login
          /* this.$vs.notification({
            color: 'danger',
            position: 'top-right',
            title: 'Registro eliminado con éxito'
          }) */
        } else { // Es Login
          /* let sessionInfo = {
            token: response.data.token
          } */
          localStorage.setItem('sessionInfo', JSON.stringify(response.data))
        }
      }
    }
    return response.data
  }, function (error) {
    // Error en la respuesta
    console.log('debug', error.response)
    if (error.response === undefined) { // Si no hubo comunicación con el servidor
      console.log('no hay conexion con el servidor', error)
      /* this.$vs.notification({
        color: 'danger',
        position: 'top-right',
        title: 'Por favor verifica tu conexión de internet'
      }) */
    } else { // Si el servidor dio respuesta
      console.log('linea49')
      if (error.response.status === 401) { // Error de Login
        /* this.$vs.notification({
          color: 'danger',
          position: 'top-right',
          title: 'Correo y/o Contraseña Incorrectos'
        }) */
      } else if (error.response.status === 403) { // Error de Login
        /* this.$vs.notification({
          color: 'danger',
          position: 'top-right',
          title: error.response.data
        }) */
      } else if (error.response.status === 404) { // Error de Login
        /* this.$vs.notification({
          color: 'danger',
          position: 'top-right',
          title: 'Error en ruta. Código 404'
        }) */
      } else if (error.response.status === 500) { // Error en servidor
        /* this.$vs.notification({
          color: 'danger',
          position: 'top-right',
          title: 'Error en servidor. Código 500'
        }) */
      }
      var data = error.response.data
      const status = error.response.status
      console.log('error.response.data.error', data)
      if (data) {
        if (status === 401) {
          console.log('data.statusCode === 401')
          if (data.code === 'LOGIN_FAILED') { // Si es un error de autenticación
            /* this.$vs.notification({
              color: 'danger',
              position: 'top-right',
              title: 'Correo y/o Contraseña Incorrectos'
            }) */
          } else {
            /* this.$vs.notification({
              color: 'danger',
              position: 'top-right',
              title: data.message
            }) */
          }
        }
        if (data.statusCode === 403) {
          /* this.$vs.notification({
            color: 'danger',
            position: 'top-right',
            title: data.message
          }) */
        }
        if (status === 422) {
          /* this.$vs.notification({
            color: 'danger',
            position: 'top-right',
            title: 'El correo ya se encentra registrado'
          }) */
          // return Promise.reject(data.response.data.error)
        }
        if (data.statusCode === 500) {
          /* this.$vs.notification({
            color: 'danger',
            position: 'top-right',
            title: 'Error interno en servidor' + data.message
          }) */
          // return Promise.reject(data.response.data.error)
        }
        // Añadir mas mensajes segun codigos de error especificos y mostrar las notificaciones correspondientes

        // Notify.create(error.response.data.error.message)
        // console.log(error.response.status);
        // console.log(error.response.headers);
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        // console.log(error.request)
      } else {
        // Something happened in setting up the request that triggered an Error
        // console.log('Error', error.message)
      }
      // console.log(error.config)
    }

    // return Promise.reject(data)
  })
    // UNa instancia para la validacion del token, en caso de utilizar login se debe usar Vuex para el store y ejecutar esta funcion
  /* instancia.interceptors.request.use(async function (config) {
    // Antes de enviar cada petición se añade el token si existe

    store.dispatch('generals/fetchAccessToken')
    const token = (store.state.generals.sessionInfo !== null) ? store.state.generals.sessionInfo.token : false
    console.log('token', token)
    if (token) {
      if (!config.headers) { config.headers = {} }
      config.headers = {
        Authorization: 'Bearer ' + token
      }
    }
    return config
  }, function (error) {
    // Do something with request error
    return Promise.reject(error)
  }) */
}

export { instancia }
