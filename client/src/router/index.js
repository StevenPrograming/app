import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: () => import('../views/About.vue')
  },
  { path: '/about', name: 'About', component: () => import('../views/About.vue') },
  { path: '/customer', name: 'List', component: () => import('../views/customer/List.vue') },
  { path: '/customer/agg_customer', name: 'List', component: () => import('../views/customer/Form.vue') },
  { path: '/customer/edit_customer/:id', name: 'List', component: () => import('../views/customer/Form.vue') },
  { path: '/report', name: 'List', component: () => import('../views/report/view.vue') },
  { path: '/city', name: 'List', component: () => import('../views/city/List.vue') },
  { path: '/city/agg_city', name: 'List', component: () => import('../views/city/Form.vue') },
  { path: '/city/edit_city/:id', name: 'List', component: () => import('../views/city/Form.vue') }
]

const router = new VueRouter({
  routes
})

export default router
